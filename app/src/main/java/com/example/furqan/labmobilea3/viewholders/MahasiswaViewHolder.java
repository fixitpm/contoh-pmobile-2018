package com.example.furqan.labmobilea3.viewholders;


import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.furqan.labmobilea3.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MahasiswaViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.rootView)
    public View rootView;

    @BindView(R.id.imageViewPhoto)
    public ImageView imageViewPhoto;

    @BindView(R.id.textViewNama)
    public TextView textViewNama;

    @BindView(R.id.textViewNim)
    public TextView textViewNim;

    public MahasiswaViewHolder(View itemView){
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
